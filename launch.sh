#!/bin/bash

SRCDIR=$1


gnome-terminal --geometry 115x53+0+0   --working-directory=$SRCDIR -- bash -c "vim ; bash"
gnome-terminal --geometry 80x24+1024+0 --working-directory=$SRCDIR -- bash -c "git pull ; bash"

